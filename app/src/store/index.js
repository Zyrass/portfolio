import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    disponibility: true,
    poste: "développeur JavaScript Intermédiaire",
  },
  mutations: {},
  actions: {},
  modules: {},
});
